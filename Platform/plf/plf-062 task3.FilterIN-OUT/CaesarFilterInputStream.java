/*
Написать классы-потоки изменяющие нижележащий поток байт прибавляя(убавляя) к каждому байту некоторое
число в соответствии с шифром цезаря.
 */

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CaesarFilterInputStream extends FilterInputStream {
    private final int SHIFT = 2;

    protected CaesarFilterInputStream(InputStream in) {
        super(in);
    }

    @Override
    public int read() throws IOException {
        return in.read() + SHIFT;
    }
}
