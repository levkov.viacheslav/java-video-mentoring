import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

public class CaesarFilterOutputStream extends FilterOutputStream {
    private final int SHIFT = 2;

    public CaesarFilterOutputStream(OutputStream out) {
        super(out);
    }

    @Override
    public void write(byte[] b) throws IOException {
        byte[] buffer = new byte[b.length];
        buffer = Arrays.copyOf(b,b.length);
        for(int i = 0; i < buffer.length; i++) {
            buffer[i] -= SHIFT;
        }
        out.write(buffer);
    }
}
