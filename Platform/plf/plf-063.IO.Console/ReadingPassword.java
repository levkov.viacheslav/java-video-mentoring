/*
Программа запрашивает логин и пароль, после правильного ввода выдает сообщение зеленыйм цветом,
при неправильном - красным.
 */

import java.util.*;
import java.io.*;

public class ReadingPassword {
    private static Map<String, String> credentials = new HashMap<>();
    static {
        credentials.put("Alex", "1234");
        credentials.put("Slava", "4321");
    }

    public static final String BEGIN = "\u001B";
    public static final String END = "\u001B[0m;";

    public static void main(String[] args) {
        boolean logged = false;
        Console cons;
        char[] passwd;
        if((cons = System.console()) != null) {
            String login = cons.readLine("Login: ");
            if(login != null && (passwd = cons.readPassword("%s","Password: ")) != null &&
                    credentials.containsValue(login)) {
                String required = credentials.get(login);
                logged = Arrays.equals(required.toCharArray(), passwd);
                java.util.Arrays.fill(passwd, ' ');
            }
            if(logged) {
                System.out.println(BEGIN + "[32mYou are logged in." + END);
            } else {
                System.out.println(BEGIN + "[31mWrong login." + END);
            }
        } else {
            System.out.println(BEGIN + "[31mConsole don't support on this platform." + END);
        }
    }
}
