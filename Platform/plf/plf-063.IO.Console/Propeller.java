/*
Дополнить программу, чтобы она периодически выводила разноцветные сигналы: stop, caution,
 go в одной и той же строке
 */

public class Propeller {
    public static final String BEGIN = "\u001B";
    public static final String END = "\u001B[0m;";

    public static void main(String[] args) throws InterruptedException {
        String[] spinner = new String[] {BEGIN +"[31m" + "\u0008Stop" + END,
                BEGIN + "[33m" + "\u0008Caution" + END,
                BEGIN + "[32m" +"\u0008Go" + END};

        for(int i = 0; i < 1000; i++) {
            Thread.sleep(500);
            System.out.printf("%s", spinner[i % spinner.length]);
            System.out.println();
        }
    }
}
