/*
Продемонстрировать применение методов класса Collection
 */

import java.util.*;

public class CollectionsDemo {
    public static void main(String[] args) {
//        List<String> names = new LinkedList<>();
//        Collections.addAll(names, "Alex","Sam","Steve");
//        System.out.println(names);
//        Collections.addAll(names,"Mary","Jane");
//        System.out.println(names);
//        System.out.println(Collections.binarySearch(names,"Sam"));

//        Deque<Integer> numbers = new LinkedList<>();
//        numbers.add(1);
//        numbers.add(2);
//        numbers.addFirst(3);
//        System.out.println(numbers);
//        Collections.asLifoQueue(numbers).poll();
//        System.out.println(numbers);
//        List<String> list = new LinkedList<>();
//        Collections.checkedList(list, String.class).add("Anna");
//        System.out.println(list);
//        List<String> letters = new ArrayList<>();
//        letters.add("a");
//        letters.add("b");
//        letters.add("c");
//        List<String> lettersCopy = new ArrayList<>();
//        lettersCopy.add("1");
//        lettersCopy.add("2");
//        lettersCopy.add("3");
//        List<String> sublist = letters.subList(1,2);
//        Enumeration<String> e = Collections.enumeration(letters);
//        while(e.hasMoreElements()){
//            System.out.println(e.nextElement());
//        Collections.copy(lettersCopy, letters);
//        System.out.println(lettersCopy);
//        System.out.println(Collections.disjoint(letters,lettersCopy));
//        List<Integer> list2 = new LinkedList<>();
//        list2 = Collections.emptyList();
//        list2.add(2);
//        System.out.println(list2);
//
//
//        System.out.println(Collections.replaceAll(letters,"b","a"));
//        System.out.println(letters);
//        Collections.reverse(letters);
//        System.out.println(letters);
//        Collections.rotate(letters,2);
//        System.out.println(letters);
//        Collections.shuffle(letters);
//        System.out.println(letters);
//        Collections.swap(lettersCopy,0,2);
//        System.out.println(lettersCopy);
//        Set<Integer> set = new HashSet<>();
//        set.add(1);
//        set.add(2);
//        set.add(3);
//        Set<Integer> set2 = Collections.unmodifiableSet(set);
//        try{
//            set2.add(3);
//        } catch(UnsupportedOperationException e) {
//            e.printStackTrace();
//        } finally {
//            System.out.println("i cant do this((((");
//        }
//

    }
}

