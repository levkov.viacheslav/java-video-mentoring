import java.io.*;

public class Demo {
    public static void main(String[] args) {
        try (RandomInputStream fis = new RandomInputStream()) {
            fis.read();
            fis.mark(6);
            System.out.println();
            System.out.println(fis.available());
            System.out.println(fis.markSupported());
            fis.read();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
