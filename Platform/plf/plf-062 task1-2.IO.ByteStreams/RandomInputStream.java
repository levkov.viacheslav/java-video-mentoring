/*
1.Реализовать класс (extends InputStream) по генерации случайных байт в виде байтового потока длинной не менее Х,
но не более У.
2. Добавить поддержку markSupported();
 */

import java.io.*;
import java.util.Random;

public class RandomInputStream extends InputStream {
    private static final byte MIN_LENGTH = 5;
    private static final byte MAX_LENGTH = 10;
    private static byte[] bytes = randomBytesArr();
    private static int mark = 0;
    private static int count = -1;
    @Override
    public int read() throws IOException {
        if(mark != 0 && count != mark) {
            count++;
            return bytes[count];
        }

        else if(mark == 0 && count < bytes.length) {
            count++;
            return bytes[count];
        }
        else {
            count = -1;
            return count;
        }
    }
    @Override
    public int available() throws IOException {
        return bytes.length;
    }
    @Override
    public void mark(int readLimit){
        if(readLimit > MIN_LENGTH && readLimit < bytes.length) {
            mark = readLimit;
        }
    }
    @Override
    public void reset() {
        mark = 0;
    }
    @Override
    public boolean markSupported() {
        return true;
    }

    private static byte[] randomBytesArr() {
        Random random = new Random();
        byte[] randomBytesArr = new byte[random.nextInt(MAX_LENGTH - MIN_LENGTH) + MIN_LENGTH];
        random.nextBytes(randomBytesArr);
        return randomBytesArr;
    }
}
