/*
Написать программу переводчик, которая будет переводить текст, введенный
на одном языке, на другой язык согласно составленному заранее словарю
 */

import java.util.*;

public class TranslatorMain {
    public static void main(String[] args) {
        Map<String, String> words = new HashMap<>();
        words.put("dog", "собака");
        words.put("run", "бегает");
        words.put("street", "улица");
        words.put("and", "и");
        words.put("say", "говорит");
        words.put("hau-hau", "гав-гав");

        System.out.println(translatePhrase(usersInput(),words));

    }

    static String usersInput() {
        System.out.println("enter there your text...");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    static StringBuilder translatePhrase(String phrase, Map<String, String> map) {
        String[] wordsArray = phrase.split(" ");
        StringBuilder outputPhrase = new StringBuilder();
        for (String s : wordsArray) {
            if (map.containsKey(s))
                outputPhrase.append(map.get(s) + " ");
            else outputPhrase.append(s + " ");
        }
        return outputPhrase;
    }
}
